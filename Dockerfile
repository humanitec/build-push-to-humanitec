FROM gcr.io/kaniko-project/executor:debug

# Set env variables
ENV DOCKER_CONFIG=/kaniko/.docker/

# Copy test repository
COPY /example/ /example/

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /
# This is a workaround since
# RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
# does not work in Busybox
COPY bitbucket/common.sh /

ENTRYPOINT ["/pipe.sh"]
