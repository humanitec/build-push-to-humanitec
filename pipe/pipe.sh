#!/busybox/sh
#
# Enjoy continuous delivery after each build in your Bitbucket pipelines.
#

source "$(dirname "$0")/common.sh"

info "Executing the Build-Push-to-Humanitec pipe"

# Required parameters
# Token to access Humanitec. Needs to be set in an environment variable in the repository.
HUMANITEC_TOKEN=${HUMANITEC_TOKEN:?'HUMANITEC_TOKEN variable missing.'}
# Name of the organization in Humanitec.
ORGANIZATION=${ORGANIZATION:?'ORGANIZATION variable missing.'}

# Default parameters
# Name of the image in Humanitec. The id must be all lowercase letters, numbers and the "-" symbol. 
# It cannot start or end with "-". Defaults to $BITBUCKET_REPO_SLUG.
IMAGE_NAME=${IMAGE_NAME:=$BITBUCKET_REPO_SLUG}
# URL of the API to be used. Defaults to https://api.humanitec.io.
HUMANITEC_API=${HUMANITEC_API:="https://api.humanitec.io"}
# Name of the dockerfile to be used. Defaults to Dockerfile.
DOCKERFILE=${DOCKERFILE:="Dockerfile"}
# Context for Kaniko. Defaults to . (working directory).
CONTEXT=${CONTEXT:="."}

# As the user can choose their image name, we need to ensure it is a valid slug (i.e. lowercase kebab case)
if ! echo "$IMAGE_NAME" | grep -q '^[a-z0-9][a-z0-9-]*[a-z0-9]$'
then
  echo "IMAGE_NAME: \"${IMAGE_NAME}\" is not valid." 1>2
  echo "IMAGE_NAME must be all lowercase letters, numbers and the \"-\" symbol. It cannot start or end with \"-\"." 1>&2
  exit 1
fi

# We use wget because we are inside BusyBox.
CREDS=$(wget -q -O - --header="Authorization: Bearer $HUMANITEC_TOKEN" "${HUMANITEC_API}/orgs/${ORGANIZATION}/registries/humanitec/creds")

if [ $? -ne 0 ]
then
  echo "Unable to access Humanitec." 1>&2
  echo "Did you add the token to your Github Secrets? http:/docs.humanitec.com/connecting-your-ci#github-actions" 1>&2
  exit 1
fi

# tr -d '\n\r' removes all newlines (including windows style)
CREDS="$(echo $CREDS | tr -d '\n\r')"

# As we are in BusyBox, we don't have any good JSON parsing/validation tools, so let's use regex!
if ! echo "$CREDS" | grep -q '\s*"password"\s*:\s*"[^"]*".*"username"\s*:\s*"[^"]*"\|.*"username"\s*:\s*"[^"]*".*"password"\s*:\s*"[^"]*"'
then
  echo "Unexpected response fetching credentials." 1>&2
  exit 1
fi

REGISTRY_USERNAME=$(echo $CREDS | sed 's/.*"username"\s*\:\s*"\([^"]*\)".*/\1/')
REGISTRY_PASSWORD=$(echo $CREDS | sed 's/.*"password"\s*\:\s*"\([^"]*\)".*/\1/')
REGISTRY_REGISTRY=$(echo $CREDS | sed 's/.*"registry"\s*\:\s*"\([^"]*\)".*/\1/')

# Create config.json to access Humanitec registry
echo "{\"auths\":{\"https://"$REGISTRY_REGISTRY"\":{\"username\":\""$REGISTRY_USERNAME"\",\"password\":\""$REGISTRY_PASSWORD"\"}}}" > /kaniko/.docker/config.json

# Generate tags
LOCAL_TAG=$ORGANIZATION/$IMAGE_NAME:$BITBUCKET_COMMIT
REMOTE_TAG=$REGISTRY_REGISTRY/$LOCAL_TAG

# Use Kaniko to build the image and push it to the Humanitec registry
executor --context=$CONTEXT --dockerfile=$DOCKERFILE --destination=$REMOTE_TAG

if [ $? -ne 0 ]
then
  echo "Unable to build and push image using Kaniko." 1>&2
  exit 1
fi

# Inform Humanitec about new build
MODULE_BUILD_INFO="{\"image\":\"${REMOTE_TAG}\",\"commit\":\"${BITBUCKET_COMMIT}\",\"branch\": \"${BITBUCKET_BRANCH}\",\"tags\":[\"${BITBUCKET_TAG}\"]}"
wget -q -O - --header="Authorization: Bearer $HUMANITEC_TOKEN" --header="Content-Type: application/json" --post-data="${MODULE_BUILD_INFO}" \
  "${HUMANITEC_API}/orgs/${ORGANIZATION}/modules/${IMAGE_NAME}/builds"

if [ $? -ne 0 ]
then
  echo "Unable to notify Humanitec of new image." 1>&2
  exit 1
fi

status=$?
if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi