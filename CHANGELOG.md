# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.9

- patch: Internal maintenance: add pipe's category to the metadata.

## 0.2.8

- patch: Minor fix in pipe

## 0.2.7

- patch: Changed maintainer formatting

## 0.2.6

- patch: Added additional information to yaml file

## 0.2.5

- patch: Change Humanitec notification

## 0.2.4

- patch: Updated README.md

## 0.2.3

- patch: Updated README.md

## 0.2.2

- patch: Switched from debug to latest

## 0.2.1

- patch: Updated README.md

## 0.2.0

- minor: 1st release for external testing

## 0.1.8

- patch: Inform platform

## 0.1.7

- patch: Removed alpine

## 0.1.6

- patch: Changed context

## 0.1.5

- patch: Re-added executor

## 0.1.4

- patch: Added ls

## 0.1.3

- patch: Added context

## 0.1.2

- patch: Changed variables

## 0.1.1

- patch: First master version

## 0.1.0

- minor: Initial release

