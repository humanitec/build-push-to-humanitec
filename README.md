# Bitbucket Pipelines Pipe: Build and push to Humanitec

Enjoy continuous delivery after each build in your Bitbucket pipelines.

This pipe uses [Kaniko](https://github.com/GoogleContainerTools/kaniko) to build and push an image to Humanitec.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: humanitec/build-push-to-humanitec:0.2.9
    variables:
      HUMANITEC_TOKEN: $HUMANITEC_TOKEN
      ORGANIZATION: "<string>"
      IMAGE_NAME: "<string>" # Optional
      HUMANITEC_API: "<string>" # Optional
      DOCKERFILE: "<string>" # Optional
      CONTEXT: "<string>" # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| HUMANITEC_TOKEN (*)   | Token to access Humanitec. Needs to be set in an environment variable in the repository. |
| ORGANIZATION (*)      | Name of the organization in Humanitec. |
| IMAGE_NAME            | Name of the image in Humanitec. The id must be all lowercase letters, numbers and the "-" symbol. It cannot start or end with "-". Defaults to $BITBUCKET_REPO_SLUG. |
| HUMANITEC_API         | URL of the API to be used. Defaults to https://api.humanitec.io. |
| DOCKERFILE            | Name of the dockerfile to be used. Defaults to Dockerfile. |
| CONTEXT               | Context for Kaniko. Defaults to . (working directory). |

_(*) = required variable._

## Prerequisites

To connect a Bitbucket pipeline and Humanitec, you need to create an access token in Humanitec. The access token has to be stored in an **environment variable** called `HUMANITEC_TOKEN` in your Bitbucket repository.

Please check out the [how-to guide in our documentation](https://docs.humanitec.com/how-to-guides/ci/bitbucket-pipelines/) for more details. 

## Examples

Basic example:

```yaml
script:
  - pipe: humanitec/build-push-to-humanitec:0.2.9
    variables:
      HUMANITEC_TOKEN: $HUMANITEC_TOKEN
      ORGANIZATION: "humanitec-demo"
```

Advanced example:

```yaml
script:
  - pipe: humanitec/build-push-to-humanitec:0.2.9
    variables:
      HUMANITEC_TOKEN: $HUMANITEC_TOKEN
      ORGANIZATION: "humanitec-demo"
      IMAGE_NAME: "example"
      DOCKERFILE: "Dockerfile_debug"
      CONTEXT: "example"
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by christoph@humanitec.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
