#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/build-push-to-humanitec"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
    run docker run \
        -e HUMANITEC_TOKEN=$HUMANITEC_TOKEN \
        -e ORGANIZATION="christophs-test-org" \
        -e CONTEXT="example" \
        -e IMAGE_NAME="example" \
        -v $(pwd):$(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

