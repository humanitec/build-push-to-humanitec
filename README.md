***OBSOLETE: This Gitlab CI job is obsolete. Please visit [our documentation](https://docs.humanitec.com/how-to-guides-devops/infrastructure-orchestration/connect-ci-pipelines) for more information.***

# Build Push To Humanitec

This Gitlab CI job allows for an easy integration of Humanitec into your existing CI processes. It supports retrieving the Humanitec registry credentials, tagging and pushing an image to the Humanitec registry, and informing Humanitec that a new build is available.

## Requirements

You need to set the `HUMANITEC_TOKEN` (API token to access your Humanitec organization) and the `HUMANITEC_ORG` (the name of your organization in Humanitec) in your Gitlab group's or project's CI/CD pipeline settings.  

## Variables

### IMAGE_NAME

_Optional_ The name you want to refer to the image to in the Humanitec Platform. It must be all lowercase letters, numbers and the "-" symbol. It cannot start or end with "-". Default to `CI_PROJECT_NAME`.

### IMAGE_TAG
_Optional_ The tag for the docker image to be tagged with. Default to `latest`.

## Usage

You can `include` the Humanitec template into the `gitlab-ci.yml` file of your project.

```yaml
stages:
  - get-humanitec-registry-credentials
  - build-push-to-humanitec
  - notify-humanitec

include:
  - remote: 'https://gitlab.com/humanitec/build-push-to-humanitec/-/raw/master/build-push-to-humanitec-template.yml'

get-humanitec-registry-credentials:
  extends: .get-humanitec-registry-credentials-template

build-push-to-humanitec:
  extends: .build-push-to-humanitec-template
  script:
    - docker build .

notify-humanitec:
  extends: .notify-humanitec-template
```

## Credentials

This integration was developed based on a community project initaited by [Antoine Rougeot](https://gitlab.com/tony-engineering).